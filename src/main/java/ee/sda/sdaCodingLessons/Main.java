package ee.sda.sdaCodingLessons;

public class Main {

    // Entry point
    public static void main(String[] args) {


        // Matrix

        /**
         * 1 2 2
         * 3 3 5
         * 4 3 3
         */
        int[][] doubleArray = new int[3][3];

        // row = 0, col = 0;
        doubleArray[0][0] = 1;
        doubleArray[0][1] = 2;
        doubleArray[0][2] = 2;

        doubleArray[1][0] = 3;
        doubleArray[1][1] = 3;
        doubleArray[1][2] = 5;

        doubleArray[2][0] = 4;
        doubleArray[2][1] = 3;
        doubleArray[2][2] = 3;

        // Outer
        for (int i = 0; i < doubleArray.length; i++) {
            // Inner, go by each column
            for (int j = 0; j < doubleArray[i].length; j++) {
                System.out.print(doubleArray[i][j] + " ");
            }
            System.out.println();
        }

    }

}
