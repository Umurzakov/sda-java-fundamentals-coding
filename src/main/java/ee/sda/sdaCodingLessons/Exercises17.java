package ee.sda.sdaCodingLessons;

import java.util.Scanner;

public class Exercises17 {

    public final static int MAX_ELEMENT = 20;

    public void solution(){
        long binaryFirst, binarySecond;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Insert first binary:");
        binaryFirst = scanner.nextLong();
        System.out.println("Insert second binary:");
        binarySecond = scanner.nextLong();

        int i = 0;

        int[] result = addTwoBinaryNumbers(binaryFirst, binarySecond, i);

        --i;

        printResult(i, result);
    }

    private void printResult(int i, int[] result) {
        System.out.println("Sum of two binary numbers: ");
        while (i >= 0){
            System.out.println(result[i--]);
        }
    }

    /// more than 150 chars
    // Make number of params up to 4, if it is 2 perfect!
    private int[] addTwoBinaryNumbers(long binaryFirst,
                                      long binarySecond,
                                      int i) {
        int[] sum = new int[MAX_ELEMENT];

        int reminder = 0;

        while (binaryFirst != 0 || binarySecond != 0){

            sum[i++] = (int)((binaryFirst % 10 + binarySecond % 10 + reminder) % 2);
            reminder = (int)((binaryFirst % 10 + binarySecond % 10 + reminder) / 2);

            binaryFirst = binaryFirst / 10;
            binarySecond = binarySecond / 10;
        }

        if (reminder != 0){
            sum[i++] = reminder;
        }
        return sum;
    }
}
