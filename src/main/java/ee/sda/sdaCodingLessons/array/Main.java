package ee.sda.sdaCodingLessons.array;

import ee.sda.sdaCodingLessons.oopandarray.EmployeeSort;

public class Main {

    public static void main(String[] args) {

        // Array, called like data structure
        // Array is index based

        // Empty array with 10 elements
        int[] array1 = new int[10];
        // Full array with 5 elements
        int[] array2 = {2,4,3,8,6};

        array1[0] = 2;
        array1[1] = 4;
        array1[2] = 3;
        array1[3] = 8;
        array1[4] = 6;

//        System.out.println(array1[4]);

        String[] employeeFirstNames = {"John", "Tom", "Jerry"};
        String[] employeeLastNames = {"LastName1", "LastName2", "LastName3"};

        double[] topSpeedLimits = {200, 300, 350.5};

        TraverseArray traverseArray = new TraverseArray();
        traverseArray.printArrayElements(array2);


    }
}
