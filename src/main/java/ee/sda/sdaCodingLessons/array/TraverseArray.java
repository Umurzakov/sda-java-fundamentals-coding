package ee.sda.sdaCodingLessons.array;

public class TraverseArray {

    public void printArrayElements(int[] arr){
        for (int i = 0; i < arr.length; i++) {
            System.out.println(i + "th position is " + arr[i]);
        }
    }
}
