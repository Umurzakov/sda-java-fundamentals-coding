package ee.sda.sdaCodingLessons;

public class DoNotRepeatYourSelf {

    // DRY
    public int sumTwoValues(int a,
                            int b){
        return a + b;
    }

    public int sumTwoValuesAndMultiplyByThree(int a,
                            int b){
        return sumTwoValues(a, b) * 3;
    }
}
