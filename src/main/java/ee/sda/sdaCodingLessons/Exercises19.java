package ee.sda.sdaCodingLessons;

import java.util.Scanner;

public class Exercises19 {

    /**
     * This method converts decimal number to
     * binary format and prints it to the console.
     *
     * @return void
     */
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter decimal number:");
        int decimalNumber = scanner.nextInt();

        // it is quotient like reminder.
        int quotient = decimalNumber, i = 1;

        int[] binaryNumber = new int[100];

        while (quotient != 0){
            binaryNumber[i++] = quotient % 2;
            quotient = quotient / 2;
        }

        System.out.println("Binary number is:");

        for (int j=i-1; j>0; j--) {
            System.out.print(binaryNumber[j] + " ");
        }
    }
}
