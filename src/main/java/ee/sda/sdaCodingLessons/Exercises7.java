package ee.sda.sdaCodingLessons;

import java.util.Scanner;

public class Exercises7 {

    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a number:");

        int num = scanner.nextInt();

        for (int i = 0; i < 10; i++) {
            // 8x1 = 8
            System.out.println(num + " x " + (i+1) +
                    " = " + (num * (i+1)));
        }

        // 001000 111
        // 1010101010101010101010101010101010101010101010101010
    }
}
