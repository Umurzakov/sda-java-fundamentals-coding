package ee.sda.sdaCodingLessons.oop;

public final class ImmutableClass {

    private static double x = 4.0; // (3.4, 5.8)
    private static double y = 5.0;

    private ImmutableClass() {
    }

    //.....
    //....

    public static double getX() {
        return x;
    }

    public static double getY() {
        return y;
    }
}
