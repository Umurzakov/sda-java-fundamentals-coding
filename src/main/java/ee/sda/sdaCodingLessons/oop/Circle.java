package ee.sda.sdaCodingLessons.oop;

public class Circle extends Shape {

    public void draw() {
        System.out.println("Circle!");
    }

    // Annotation
    public void clean() {

    }
}
