package ee.sda.sdaCodingLessons.oop;

public class Cat {
    // An object is an entity that has states and
    // behaviors

    // States
    // Mandatory fields
    private String name;
    private int age;
    private String color;
    // Optional fields
    private String price;

    public Cat() {
    }

    public Cat(String name,
               int age,
               String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    public Cat(String name, int age, String color, String price) {
        this.name = name;
        this.age = age;
        this.color = color;
        this.price = price;
    }

    // Behaviors
    public void sleep(){
        System.out.println("Sleeping");
    }

    public void play(){
        System.out.println("Playing");
    }

    // Read only
    public String getName(){
        return this.name;
    }

    public int getAge() {
        return age;
    }

    public String getColor() {
        return color;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
