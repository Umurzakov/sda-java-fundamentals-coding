package ee.sda.sdaCodingLessons.oop;

abstract class Shape {

    // Draws something.
    public abstract void draw();

    public abstract void clean();
}
