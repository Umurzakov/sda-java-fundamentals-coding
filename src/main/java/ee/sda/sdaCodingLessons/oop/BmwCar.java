package ee.sda.sdaCodingLessons.oop;

public class BmwCar extends Car {

    private String version;

    public BmwCar(String name,
                  String color,
                  String version) {
        // Super calls parent's constructor
        super(name, color);
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


}
