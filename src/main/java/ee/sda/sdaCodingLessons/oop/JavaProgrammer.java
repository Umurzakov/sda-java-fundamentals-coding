package ee.sda.sdaCodingLessons.oop;

public class JavaProgrammer extends Programmer{
    // Polymorphism - getting many forms
    // 1. Overriding
    // 2. Overloading

    // Aggregation
    // Composition

    // A and B class, HAS-A

    @Override
    public void code() {
        System.out.println("Coding in C++");
    }

    public static void staticMethod(){

    }

    public void simpleMethod(){

    }
}
