package ee.sda.sdaCodingLessons.oop;

// Base, parent class
abstract class Motorcycle {

    // OOP principles:
    // 1. Encapsulation
    // 2. Inheritance
    // 3. Polymorphism
    // 4. Abstraction

    // Properties or States
    private String model;
    private String color;
    private double speed;
    // The rest of fields

    //Behavior
    public int drive(int km, int speed){
        this.speed = speed;
        return km/speed;
    }

    // Overloading
    public int drive(int speed){
        this.speed = speed;
        return 2;
    }

    protected void addGas(int gallons){

    }
    abstract double getTopSpeed();

    public void setModel(String model) {
        this.model = model;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public double getSpeed() {
        return speed;
    }
}
