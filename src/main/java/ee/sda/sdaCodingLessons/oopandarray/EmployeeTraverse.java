package ee.sda.sdaCodingLessons.oopandarray;

import ee.sda.sdaCodingLessons.oop.Employee;

public class EmployeeTraverse {

    // Show all employee's first names

    public void showAllEmployeesFirstName(Employee[] employees){
        System.out.println("We have these employees: ");
        System.out.println();

        for (int i = 0; i < employees.length; i++) {
            if(employees[i] != null) {
                System.out.print(employees[i].getFirstName() + " age: "
                        +employees[i].getAge() + ", ");
            }
        }
    }
}
