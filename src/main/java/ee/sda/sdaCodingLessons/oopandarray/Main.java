package ee.sda.sdaCodingLessons.oopandarray;

import ee.sda.sdaCodingLessons.oop.Company;
import ee.sda.sdaCodingLessons.oop.Employee;

import static ee.sda.sdaCodingLessons.oop.Company.MAX_NUM_EMPLOYEES;

public class Main {

    public static void main(String[] args) {
        Company newCompany = new Company("BMW");

        Employee employee1 = new Employee("John", "John's last name", 32);
        Employee employee2 = new Employee("Smith", "Smith's last name", 23);
        Employee employee3 = new Employee("Jerry", "Jerry's last name", 40);
        Employee employee4 = new Employee("Born", "Born's last name", 38);

        Employee[] employeeList = new Employee[MAX_NUM_EMPLOYEES];
        employeeList[0] = employee1;
        employeeList[1] = employee2;
        employeeList[2] = employee3;
        employeeList[3] = employee4;

        newCompany.setEmployees(employeeList);

        // Show all employees first name
        EmployeeTraverse employeeTraverse = new EmployeeTraverse();
        employeeTraverse.showAllEmployeesFirstName(newCompany.getEmployees());

        //
        EmployeeSearch employeeSearch = new EmployeeSearch();
        Employee foundEmployee = employeeSearch.findEmployeeByFirstName(employeeList, "Smith");

        // NPE we should handled
        if(foundEmployee != null){
            System.out.println();

            System.out.println("Found employee's age is ");
            System.out.println(foundEmployee.getAge());
        }

        // Insert new employee
        EmployeeInsert employeeInsert = new EmployeeInsert();
        Employee employee5 = new Employee("Mark", "Mark's last name", 57);

        employeeInsert.addNewEmployee(employeeList, employee5);

        // Show all employees again after insertion
        System.out.println();
        System.out.println();

        System.out.println("New updated list is after we inserted: ");
        employeeTraverse.showAllEmployeesFirstName(employeeList);

        System.out.println();
        System.out.println();

        System.out.println("New updated list is after we deleted given employee: ");
        EmployeeDelete employeeDelete = new EmployeeDelete();
        employeeDelete.deleteEmployee(employeeList, employee5);

        employeeTraverse.showAllEmployeesFirstName(employeeList);


        // Now we can try to sort employees by their ages.
        EmployeeSort employeeSort = new EmployeeSort();
        Employee[] sortedEmployees = employeeSort.sortEmployeesByAges(employeeList);

        System.out.println();
        System.out.println();

        System.out.println("New updated list is after we sorted employees by their ages: ");

        employeeTraverse.showAllEmployeesFirstName(sortedEmployees);

        Employee[] employeeListForBDepartment = new Employee[MAX_NUM_EMPLOYEES];

        employeeListForBDepartment[0] = new Employee("TestFirstname2", "TestLastName2", 56);
        employeeListForBDepartment[1] = new Employee("TestFirstname3", "TestLastName3", 29);
        employeeListForBDepartment[2] = new Employee("TestFirstname4", "TestLastName4", 42);

        // Merge operation
        // employeeList + employeeListForBDepartment

        EmployeeMerge employeeMerge = new EmployeeMerge();
        Employee[] mergedEmployees = employeeMerge.mergeTwoEmployeesList(sortedEmployees, employeeListForBDepartment);

        System.out.println();
        System.out.println();

        System.out.println("New updated list is after we merged two department's employees: ");
        employeeTraverse.showAllEmployeesFirstName(mergedEmployees);

        employeeSort.sortEmployeesByAges(mergedEmployees);

        employeeTraverse.showAllEmployeesFirstName(mergedEmployees);
    }
}
