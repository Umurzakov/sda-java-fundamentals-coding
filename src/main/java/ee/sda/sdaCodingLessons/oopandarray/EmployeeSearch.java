package ee.sda.sdaCodingLessons.oopandarray;

import ee.sda.sdaCodingLessons.oop.Employee;

public class EmployeeSearch {

    public Employee findEmployeeByFirstName(Employee[] employees,
                                            String firstName){
        for (int i = 0; i < employees.length; i++) {
            // John, John
             if(employees[i].getFirstName().equals(firstName)){
                 return employees[i];
             }
        }
        return null;
    }

    // Add more methods to search
}
