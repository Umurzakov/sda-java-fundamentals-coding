package ee.sda.sdaCodingLessons.oopandarray;

import ee.sda.sdaCodingLessons.oop.Employee;

public class EmployeeInsert {

    public Employee[] addNewEmployee(Employee[] employees,
                                     Employee newEmployee){

        // John, Smith, Jerry, Born, Mark, null, null, null, null, null
            for (int i = 0; i < employees.length; i++) {
                if(employees[i] == null){
                    employees[i] = newEmployee;
                    break;
                }
            }
        return employees;
    }
}
