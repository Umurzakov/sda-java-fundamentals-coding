package ee.sda.sdaCodingLessons.oopandarray;

import ee.sda.sdaCodingLessons.oop.Employee;

public class EmployeeDelete {

    public boolean deleteEmployee(Employee[] employees,
                                  Employee employeeRemoval){
        for (int i = 0; i < employees.length; i++) {
            if(employees[i] != null
                    && employees[i].getFirstName().equals(employeeRemoval.getFirstName())){
                employees[i] = null;
                return true;
            }
        }
        return false;
    }
}
